#! /bin/sh

# 自己的war文件的目录
backup_war_dir="/data/missj/lottery/wars"
#
jar_name="agency" 
jar_dir_name="agency_8088"
# jar运行目录
jar_dir="/data/missj/lottery/lottery/agency_8088"

if [ ! -d ${jar_dir} ]; then
	echo "tomcat: ${jar_dir} not installed..."
	exit 1
fi
if [ ! -d ${backup_war_dir} ]; then
	echo "war_dir: ${backup_war_dir} not exist. create it..."
	mkdir -p ${backup_war_dir}
fi

# 1、stop proc
# shellcheck disable=SC2009
procs=$(ps aux |grep "${jar_dir_name}" | grep -v "grep" | awk '{print $2}')
echo "stop procs..."
for my_proc in ${procs}; do
	kill "${my_proc}"
done
sleep 5

# 2、ready package to deploy
echo "ready war to deploy..."
if [ ! -f ${backup_war_dir}/${jar_name}.jar ]; then
	echo "war file: ${jar_name}.jar not exist..."
	exit 1
fi
now_time=$(date +"%Y%m%d_%H%M%S")
# backup war to now_time
cp ${backup_war_dir}/${jar_name}.jar ${backup_war_dir}/${jar_name}.${now_time}.jar
# 3、startup.sh
echo "start ${war_suffix} ..."
cd ${jar_dir}/
rm -rf ${jar_dir_name}*
cp -rf ${backup_war_dir}/${jar_name}.jar ./

nohup $JRE_HOME/bin/java -Xms256m -Xmx512m -jar ${jar_dir}/${jar_name}.jar >/dev/null 2>&1 &
# shellcheck disable=SC2009
ps aux |grep "${jar_dir_name}" | grep -v "grep"

