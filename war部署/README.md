> 该项目是本地打包，服务器执行脚本

## 使用教程

### windows本地安装putty

### 修改批处理脚本deploy_*.bat

* project_path 本地项目所在路径
> set project_path=D:\missj\lottery\trunk\agencyadmin

* 服务器用户名
server_user_1=root

* 服务器密码
set server_passwd_1=admin

* 服务器ip地址
set server_ip_1=192.168.0.100

* jar name
jar_name=agency.jar

* 远程服务器war备份地址
server_path=/data/missj/lottery/wars

* 远程服务器shell脚本所在路径
set deploy-shell=/data/missj/lottery/opt/deploy_agency.sh


* 远程服务器ssh登陆端口
set port=22

### 修改shell脚本deploy_*.sh
# 自己的war文件的目录
backup_war_dir="/data/missj/lottery/wars"
#
war_name="admin"
tomcat_name="admin_8083"
# tomcat运行目录
tomcat_dir="/data/missj/lottery/lottery/admin_8083"