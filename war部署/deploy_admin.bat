@echo off

:: project dir
set project_path=D:\missj\lottery\trunk\admin_master

:: username of the server
set server_user_1=root
:: password of the server
set server_passwd_1=admin
:: ip of the server
set server_ip_1=192.168.0.100
:: path of the project's war
set local_file1=%project_path%\target\admin.war
:: temp dir which is used to save the war
set server_path=/data/missj/lottery/wars
set deploy-shell=/data/missj/lottery/opt/deploy_admin_war.sh
set port=22

echo JDK version currently in use: 
java -version

echo begin to build the war
set /p env=package environment(dev test panda huanle,exit):

D:
cd %project_path%
call mvn clean install -P %env%
echo build success
echo begin to upload %local_file1% to %server_ip_1%
:: need to install putty
call pscp -P %port% -l %server_user_1% -pw %server_passwd_1% -r %local_file1% %server_ip_1%:%server_path%
echo upload success

echo begin to execute bash
call plink -P %port% %server_user_1%@%server_ip_1% -pw %server_passwd_1% sh %deploy-shell%

pause