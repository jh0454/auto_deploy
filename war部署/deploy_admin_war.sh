#! /bin/sh

# 自己的war文件的目录
backup_war_dir="/data/missj/lottery/wars"
#
war_name="admin"
tomcat_name="admin_8083"
# tomcat运行目录
tomcat_dir="/data/missj/lottery/lottery/admin_8083"

if [ ! -d ${tomcat_dir} ]; then
	echo "tomcat: ${tomcat_dir} not installed..."
	exit 1
fi
if [ ! -d ${backup_war_dir} ]; then
	echo "war_dir: ${backup_war_dir} not exist. create it..."
	mkdir -p ${backup_war_dir}
fi

# 1、stop proc
# shellcheck disable=SC2009
procs=$(ps aux |grep "${tomcat_name}" | grep -v "grep" | awk '{print $2}')
echo "stop procs..."
for my_proc in ${procs}; do
	kill "${my_proc}"
done
sleep 5

# 2、ready package to deploy
echo "ready war to deploy..."
if [ ! -f ${backup_war_dir}/${war_name}.war ]; then
	echo "war file: ${war_name}.war not exist..."
	exit 1
fi
now_time=$(date +"%Y%m%d_%H%M%S")
# backup war to now_time
cp ${backup_war_dir}/${war_name}.war ${backup_war_dir}/${war_name}.${now_time}.war
cd ${backup_war_dir}
if [ -d ${war_name} ]; then
	rm -rf ${war_name}
fi
unzip -d ${war_name} ${war_name}.war

# 3、startup.sh
echo "start {war_name} ..."
cd ${tomcat_dir}/webapps/
rm -rf ${war_name}*
cp -rf ${backup_war_dir}/${war_name} ./

cd ${tomcat_dir}/bin/ && ./startup.sh
# shellcheck disable=SC2009
ps aux |grep "${tomcat_name}" | grep -v "grep"

